const prefix = "movieList";

export const FETCH_MOVIES = `${prefix}/FETCH_MOVIES`;
export const FETCH_MOVIES_COMPLETE = `${prefix}/FETCH_MOVIES_COMPLETE`;
export const FETCH_MOVIES_FAILED = `${prefix}/FETCH_MOVIES_FAILED`;
