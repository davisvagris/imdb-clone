import {
  FETCH_MOVIES,
  FETCH_MOVIES_COMPLETE,
  FETCH_MOVIES_FAILED
} from "./constants";

const initialState = {
  loading: false,
  movies: null,
  error: null
};

export const movieList = (state = initialState, action) => {
  console.log(action.type, action.payload)
  switch (action.type) {
    case FETCH_MOVIES:
      return {
        ...state,
        loading: true,
        movies: null,
        error: null
      };

    case FETCH_MOVIES_COMPLETE:
      return {
        ...state,
        loading: false,
        movies: action.payload,
        error: null
      };

    case FETCH_MOVIES_FAILED:
      return {
        ...state,
        loading: false,
        movies: null,
        error: action.payload
      };

    default:
      return state;
  }
};
