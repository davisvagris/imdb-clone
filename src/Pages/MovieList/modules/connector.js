import {
  FETCH_MOVIES,
  FETCH_MOVIES_COMPLETE,
  FETCH_MOVIES_FAILED
} from "../../../reducers/constants";
import { call, put, takeEvery, all, fork, takeLatest, takeLeading } from "redux-saga/effects";

export const fetchMoviesStart = (search) => {
  return {
    type: FETCH_MOVIES,
      payload: {
        search
      }
  };
};



const fetchMovies = async (payload) => {
    console.log('search', payload);

    const result = await fetch(
        "http://www.omdbapi.com/?s=work&apikey=86aa0d18"
    );

    return await result.json();
};

function* getMovies({payload}) {

    try {
    const movies = yield call(fetchMovies, payload);

    yield put({
        type: FETCH_MOVIES_COMPLETE,
        payload: movies
    });
  } catch (e) {
        throw e;
    yield put({
      type: FETCH_MOVIES_FAILED
    });
  }
}

export default function* watchMovies() {
  yield takeLeading(FETCH_MOVIES, getMovies);
}
