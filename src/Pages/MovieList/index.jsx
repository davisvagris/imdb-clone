import React from "react";
import { Movie } from "./Components/Movie";
import { connect } from "react-redux";
import { fetchMoviesStart } from "./modules/connector";

export class MovieList extends React.Component {
  componentDidMount() {
    this.props.fetchMoviesStart("simpson");
  }

  render() {
    const { movies } = this.props;
    return <>
        <button onClick={() => this.props.fetchMoviesStart("simpson")}>Hi</button>
        {movies && movies.map(movie => <Movie key={movie.title} {...movie} />)}</>;
  }
}

const mapStateToProps = props => {

  if (props.movies) {
     const movies = props.movies.Search.map(({Title}) => {
      return {
        title: Title,
      }
    });

     return {
       movies
     }
  }

  return props;
};

export const MovieListConnected = connect(
  mapStateToProps,
  { fetchMoviesStart }
)(MovieList);
